package com.pppb.tugasbesar03_pppb;

public interface FragmentListener {
    void changePage(int page);
    void closeApplication();
}

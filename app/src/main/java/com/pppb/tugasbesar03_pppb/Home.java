package com.pppb.tugasbesar03_pppb;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

public class Home extends Fragment implements View.OnClickListener{
    private FragmentListener fragmentlistener;

    public Home(){}
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.home, container, false);

        return view;
    }

    public static Home newInstance(){
        Home fragment = new Home();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof FragmentListener){
            this.fragmentlistener = (FragmentListener) context;
        }
        else{
            throw new ClassCastException(context.toString()+ " must implement FragmentListener");
        }
    }

    @Override
    public void onClick(View view) {

    }
}

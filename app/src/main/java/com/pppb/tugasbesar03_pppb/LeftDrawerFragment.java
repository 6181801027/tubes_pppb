package com.pppb.tugasbesar03_pppb;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class LeftDrawerFragment extends Fragment implements View.OnClickListener{
    private TextView tvhome, tvnews, tvprice, tvsettings, tvexit;
    private FragmentListener fragmentlistener;

    public LeftDrawerFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.left_drawer, container, false);

        this.tvhome = view.findViewById(R.id.tv_home);
        this.tvnews = view.findViewById(R.id.tv_news);
        this.tvprice = view.findViewById(R.id.tv_price);
        this.tvsettings = view.findViewById(R.id.tv_setting);
        this.tvexit = view.findViewById(R.id.tv_exit);

        this.tvhome.setOnClickListener(this);
        this.tvnews.setOnClickListener(this);
        this.tvexit.setOnClickListener(this);
        this.tvprice.setOnClickListener(this);
        this.tvsettings.setOnClickListener(this);
        this.tvexit.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof FragmentListener){
            this.fragmentlistener = (FragmentListener) context;
        }
        else{
            throw new ClassCastException(context.toString()+ " must implement FragmentListener");
        }
    }

    @Override
    public void onClick(View view) {
        if(view == this.tvhome){
            fragmentlistener.changePage(1);
        }
        else if (view == this.tvnews){
            fragmentlistener.changePage(2);
        }
        else if (view == this.tvprice){
            fragmentlistener.changePage(3);
        }
        else if (view == this.tvsettings){
            fragmentlistener.changePage(4);
        }
        else if (view == this.tvexit){
            fragmentlistener.closeApplication();
        }
    }
}

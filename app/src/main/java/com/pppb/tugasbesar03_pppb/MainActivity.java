package com.pppb.tugasbesar03_pppb;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.pppb.tugasbesar03_pppb.coin.CoinDetails;
import com.pppb.tugasbesar03_pppb.databinding.ActivityMainBinding;
import com.pppb.tugasbesar03_pppb.news.News;
import com.pppb.tugasbesar03_pppb.price.Price;
import com.pppb.tugasbesar03_pppb.settings.Settings;
import com.beardedhen.androidbootstrap.TypefaceProvider;

public class MainActivity extends AppCompatActivity implements FragmentListener{
    ActivityMainBinding binding;
    FragmentManager fragmentManager;


    Home home;
    News news;
    Price price;
    Settings settings;
    CoinDetails coindetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = this.binding.getRoot();
        setContentView(view);

        TypefaceProvider.registerDefaultIconSets();

        ActionBarDrawerToggle abdt = new ActionBarDrawerToggle(this, this.binding.drawerLayout, this.binding.toolbar, R.string.drawer_open, R.string.drawer_close);
        this.binding.drawerLayout.addDrawerListener(abdt);
        abdt.syncState();

        this.home = home.newInstance();
        this.news = news.newInstance();
        this.price = price.newInstance();
        this.settings = settings.newInstance();

        this.fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction ft = this.fragmentManager.beginTransaction();
        ft.add(R.id.fragment_container, this.home).addToBackStack(null).commit();
    }

    @Override
    public void changePage(int page) {
        FragmentTransaction ft = this.fragmentManager.beginTransaction();
        // Home Fragment
        if(page == 1){
            ft.replace(R.id.fragment_container, this.home).addToBackStack(null);
        }
        // News Fragment
        else if (page == 2){
            ft.replace(R.id.fragment_container, this.news).addToBackStack(null);
        }
        // Price Fragment
        else if (page == 3){
            ft.replace(R.id.fragment_container, this.price).addToBackStack(null);
        }
        // Settings Fragment
        else if (page == 4){
            ft.replace(R.id.fragment_container, this.settings).addToBackStack(null);
        }
        // Coin Details Fragment
        else if (page == 5){
            this.coindetails = coindetails.newInstance(this.price.coin_fromLst);
            ft.replace(R.id.fragment_container,this.coindetails).addToBackStack(null);
        }
        ft.commit();
        this.binding.drawerLayout.closeDrawers();

    }

    @Override
    public void closeApplication() {
        this.moveTaskToBack(true);
        this.finish();
    }
}

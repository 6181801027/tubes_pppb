package com.pppb.tugasbesar03_pppb.coin;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.beardedhen.androidbootstrap.AwesomeTextView;
import com.pppb.tugasbesar03_pppb.FragmentListener;
import com.pppb.tugasbesar03_pppb.R;
import com.pppb.tugasbesar03_pppb.news.News;

public class CoinDetails extends Fragment implements View.OnClickListener {
    private FragmentListener fragmentListener;
    public TextView name,code,price,percentage,highprice,lowprice,marketprice,volumeprice;
    AwesomeTextView atwstatus;

    public CoinDetailsThread pt;
    public UIThreadedWrapperCDetails uitw;

    public CoinDetails(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.coindetails, container, false);

        this.name = view.findViewById(R.id.coin_name);
        this.name.setText(this.getArguments().getString("coin_name"));
        this.code = view.findViewById(R.id.coin_code);
        this.price = view.findViewById(R.id.coin_price);
        this.percentage = view.findViewById(R.id.coin_percentage);
        this.highprice = view.findViewById(R.id.tv_openprice);
        this.lowprice = view.findViewById(R.id.tv_closeprice);
        this.marketprice = view.findViewById(R.id.tv_marketprice);
        this.volumeprice = view.findViewById(R.id.tv_volumeprice);
        this.atwstatus = view.findViewById(R.id.status);

        initializeCoinCode();

        this.uitw = new UIThreadedWrapperCDetails(this);
        this.pt = new CoinDetailsThread(uitw,this,this.getArguments().getString("coin_name").toLowerCase());
        this.pt.startThread();

        return view;
    }

    public static CoinDetails newInstance(String coin_name){
        CoinDetails fragment = new CoinDetails();
        Bundle args = new Bundle();
        args.putString("coin_name",coin_name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            this.fragmentListener = (FragmentListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement FragmentListener");
        }
    }
    @Override
    public void onClick(View view) {

    }

    public void initializeCoinCode(){
        if(this.name.getText().equals("Bitcoin")){
            this.code.setText("BTC/USD");
        }
        else if(this.name.getText().equals("Ethereum")){
            this.code.setText("ETH/USD");
        }
        else if(this.name.getText().equals("Ripple")){
            this.code.setText("XRP/USD");
        }
        else if(this.name.getText().equals("Litecoin")){
            this.code.setText("LTC/USD");
        }
        else if(this.name.getText().equals("Cardano")){
            this.code.setText("ADA/USD");
        }
        else if(this.name.getText().equals("DogeCoin")){
            this.code.setText("DOGE/USD");
        }
        else if(this.name.getText().equals("Polkadot")){
            this.code.setText("DOT/USD");
        }
        else if(this.name.getText().equals("Stellar")){
            this.code.setText("XLM/USD");
        }
        else if(this.name.getText().equals("Chainlink")){
            this.code.setText("LINK/USD");
        }
        else if(this.name.getText().equals("BinanceCoin")){
            this.code.setText("BNB/USD");
        }
    }

}

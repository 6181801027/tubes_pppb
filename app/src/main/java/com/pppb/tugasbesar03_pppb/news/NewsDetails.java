package com.pppb.tugasbesar03_pppb.news;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.pppb.tugasbesar03_pppb.FragmentListener;
import com.pppb.tugasbesar03_pppb.R;

public class NewsDetails extends Fragment implements View.OnClickListener {
    private FragmentListener fragmentListener;
    public NewsDetails(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.newsdetails, container, false);

        return view;
    }

    public static NewsDetails newInstance(){
        NewsDetails fragment = new NewsDetails();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            this.fragmentListener = (FragmentListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement FragmentListener");
        }
    }
    @Override
    public void onClick(View view) {

    }
}

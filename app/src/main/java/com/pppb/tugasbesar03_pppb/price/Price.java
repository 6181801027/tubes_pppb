package com.pppb.tugasbesar03_pppb.price;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.beardedhen.androidbootstrap.AwesomeTextView;
import com.beardedhen.androidbootstrap.BootstrapText;
import com.beardedhen.androidbootstrap.api.attributes.BootstrapBrand;
import com.beardedhen.androidbootstrap.api.defaults.DefaultBootstrapBrand;
import com.beardedhen.androidbootstrap.font.FontAwesome;
import com.pppb.tugasbesar03_pppb.FragmentListener;
import com.pppb.tugasbesar03_pppb.R;
import com.pppb.tugasbesar03_pppb.news.News;

import java.util.ArrayList;
import java.util.List;

public class Price extends Fragment implements View.OnClickListener {
    private FragmentListener fragmentListener;
    protected ListView listView;
    public CoinListAdapter adapter;

    public PriceThread pt;
    public UIThreadedWrapperPrice uitw;
    public String coin_query = ""; //string builder untuk dimasukkan sebagai parameter pada web service coingecko
    public String coin_fromLst = ""; //value string utk mengidentifikasi mana item yg diklik pd listview

    public Price(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.price, container, false);

        this.listView = view.findViewById(R.id.lv_price);

        this.adapter = new CoinListAdapter(this, getActivity());
        this.listView.setAdapter(this.adapter);


        initializeLstData();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //String selectedFromList = (String) listView.getItemAtPosition(i);
                coin_fromLst = adapter.listCoins.get(i).getCoin_name();
                fragmentListener.changePage(5);
            }
        });


        return view;
    }

    public static Price newInstance(){
        Price fragment = new Price();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            this.fragmentListener = (FragmentListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement FragmentListener");
        }
    }
    @Override
    public void onClick(View view) {

    }

    public void initializeLstData(){
        adapter.add("Bitcoin","BTC/USD");

        adapter.add("Ethereum","ETH/USD");

        adapter.add("Ripple","XRP/USD");

        adapter.add("Litecoin","LTC/USD");

        adapter.add("Cardano","ADA/USD");

        adapter.add("DogeCoin","DOGE/USD");

        adapter.add("Polkadot","DOT/USD");

        adapter.add("Stellar","XLM/USD");

        adapter.add("Chainlink","LINK/USD");

        adapter.add("BinanceCoin","BNB/USD");

        for(int i = 0 ; i<10 ; i++){
            coin_query = coin_query + "," + this.adapter.listCoins.get(i).getCoin_name().toLowerCase();
        }

        this.uitw = new UIThreadedWrapperPrice(this);
        this.pt = new PriceThread(uitw,this,coin_query);
        this.pt.startThread();

    }

}

class Coins{
    private String coin_name;
    private String coin_code;
    private String price;
    private String percentage;

    public Coins(String coin_name,String coin_code,String price,String percentage){
        this.coin_name = coin_name;
        this.coin_code = coin_code;
        this.price = price;
        this.percentage = percentage;
    }

    public String getCoin_name(){
        return this.coin_name;
    }

    public String getCoin_code(){
        return this.coin_code;
    }

    public String getPrice(){
        return this.price;
    }

    public String getPercentage(){
        String[] temp = this.percentage.split(" ");
        float result = Float.parseFloat(temp[0]);
        String output = String.format("%.2f", result);
        return output+ "%";
    }

    public void setCoin_name(String coin_name){
        this.coin_name = coin_name;
    }

    public void setCoin_code(String coin_code){
        this.coin_code = coin_code;
    }

    public void setPrice(String price){
        this.price = "$ "+ price;
    }

    public void setPercentage(String percentage){
        this.percentage = percentage+" %";
    }
}

class ViewHolder{
    protected TextView name,code,price,percentage;
    AwesomeTextView atwstatus;
    Context context;

    public ViewHolder(View view, Context context){
        this.context = context;
        this.name = view.findViewById(R.id.item_coin);
        this.code = view.findViewById(R.id.item_code);
        this.price = view.findViewById(R.id.item_price);
        this.percentage = view.findViewById(R.id.item_percentage);
        this.atwstatus = view.findViewById(R.id.atw_status);
    }

    public void updateView(String name,String code,String price,String percentage){
        this.name.setText(name);
        this.code.setText(code);
        this.price.setText(price);
        this.percentage.setText(percentage);
        if (getCoinStatus(percentage) == 0){
            // Turun
            BootstrapText text = new BootstrapText.Builder(this.context).addText(" ").addFontAwesomeIcon(FontAwesome.FA_CHEVRON_CIRCLE_DOWN).build();
            this.atwstatus.setBootstrapText(text);
            this.atwstatus.setBootstrapBrand(DefaultBootstrapBrand.DANGER);
            this.percentage.setTextColor(this.context.getResources().getColor(R.color.red));
        }
        else if (getCoinStatus(percentage) == 1){
            // Tetap
            BootstrapText text = new BootstrapText.Builder(this.context).addText(" ").addFontAwesomeIcon(FontAwesome.FA_BARS).build();
            this.atwstatus.setBootstrapText(text);
            this.atwstatus.setBootstrapBrand(DefaultBootstrapBrand.PRIMARY);

        }
        else if (getCoinStatus(percentage) == 2){
            // Naik
            BootstrapText text = new BootstrapText.Builder(this.context).addText(" ").addFontAwesomeIcon(FontAwesome.FA_CHEVRON_CIRCLE_UP).build();
            this.atwstatus.setBootstrapText(text);
            this.atwstatus.setBootstrapBrand(DefaultBootstrapBrand.SUCCESS);
            this.percentage.setTextColor(this.context.getResources().getColor(R.color.green));
        }
    }

    /*
    * 0 berarti turun
    * 1 berarti tetap
    * 2 berarti naik
    * */
    public int getCoinStatus(String percentage){
        String temp = percentage.substring(0, percentage.length()-1);
        float result = Float.parseFloat(temp);
        if (result > 0){
            return 2;
        }
        else if (result < 0){
            return 0;
        }
        return 1;
    }

}

class CoinListAdapter extends BaseAdapter {
    List<Coins> listCoins;
    Price activity;
    Context context;

    public CoinListAdapter(Price activity, Context context){
        this.activity = activity;
        this.listCoins = new ArrayList<Coins>();
        this.context = context;
    }

    public void add(String name,String code){
        this.listCoins.add(new Coins(name,code,"0","0"));
        this.notifyDataSetChanged();
    }


    @Override
    public int getCount(){
        return listCoins.size();
    }

    @Override
    public Object getItem(int i){
        return listCoins.get(i);
    }

    @Override
    public long getItemId(int i){
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent){
        ViewHolder viewHolder;
        Coins currentCoin = (Coins)this.getItem(i);

        if (convertView == null){
            convertView = LayoutInflater.from(this.activity.getContext()).inflate(R.layout.pricelist_adapter, parent, false);
            viewHolder = new ViewHolder(convertView, context);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.updateView(currentCoin.getCoin_name(),currentCoin.getCoin_code(),currentCoin.getPrice(),currentCoin.getPercentage());

        return convertView;
    }

}




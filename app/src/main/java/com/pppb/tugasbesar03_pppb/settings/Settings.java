package com.pppb.tugasbesar03_pppb.settings;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.pppb.tugasbesar03_pppb.FragmentListener;
import com.pppb.tugasbesar03_pppb.R;
import com.pppb.tugasbesar03_pppb.price.Price;

public class Settings extends Fragment implements View.OnClickListener {
    private FragmentListener fragmentListener;
    public Settings(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.settings, container, false);

        return view;
    }

    public static Settings newInstance(){
        Settings fragment = new Settings();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentListener) {
            this.fragmentListener = (FragmentListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement FragmentListener");
        }
    }

    @Override
    public void onClick(View view) {

    }
}
